var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
users = [];
connections = [];

app.get('/', function(req, res){
  res.sendFile(__dirname + '/project/index.html');
});

io.on('connection', function(socket){
  connections.push(socket);
  console.log('a user connected');
  socket.on('disconnect', function(data){
    connections.splice(connections.indexOf(socket), 1);
    console.log('user disconnected');
  });
  socket.on('send mess', function(data){
    io.emit('add mess', {mess: data.mess, name: data.name, className: data.className});
  });
});

// io.on('connection', function(socket){
//   socket.on('chat message', function(msg){
//     io.emit('chat message', msg);
//   });
// });

http.listen(3000, function(){
  console.log('listening on *:3000');
});

